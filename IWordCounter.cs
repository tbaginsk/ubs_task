using System.Collections.Generic;

namespace UbsCodingTask
{
    public interface IWordCounter 
    {
        void CountWordsAndPrint(string input);
        Dictionary<string, int> CountWords(string input);
    }
}