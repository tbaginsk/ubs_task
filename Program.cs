﻿namespace UbsCodingTask
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var input = "This is a statement, and so is this.";
            var input1 = "We are the global team (Chicago, London, Zurich and Wroclaw)" 
                + " responsible for delivering technical and functional enhancements" 
                + " to the FX platform, architecting, designing, and coding enterprise" 
                + " applications and tools using various Microsoft and third party technologies.";
                
            var wordCounter = new WordCounter();
            wordCounter.CountWordsAndPrint(input);
            wordCounter.CountWordsAndPrint(input1);
        }
    }
}
