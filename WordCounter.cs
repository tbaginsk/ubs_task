using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace UbsCodingTask
{
    public class WordCounter : IWordCounter
    {
        public void CountWordsAndPrint(string input)
        {
            var wordCounts = CountWords(input);
            foreach (var w in wordCounts)
            {
                Console.WriteLine(string.Format("{0} - {1}", w.Key, w.Value));
            }
        }
        
        public Dictionary<string, int> CountWords(string input)
        {
            input = RemoveSymbols(input);
            input = input.ToLower();
            return CountWordsOccurrences(input);
        }

        private string RemoveSymbols(string input)
        {
            Regex rgx = new Regex("[^a-zA-Z -]");
            return rgx.Replace(input, "");
        }

        private Dictionary<string, int> CountWordsOccurrences(string input)
        {
            var allWords = input.Split(' ');
            var wordOccurrences = new Dictionary<string, int> ();
            foreach (var word in allWords)
            {
                if (!wordOccurrences.ContainsKey(word))
                    wordOccurrences[word] = 1;
                else
                    wordOccurrences[word]++;
            }
            return wordOccurrences;
        }
    }
}